function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    %







    % ============================================================
    temp_theta = theta;

    %Computes how much the jth theta element will descend in this iteration.
    %Together all j thetas will determine how much we will descend along the 
    %cost function J.
    for j = 1:length(theta)
        temp_theta(j, :) -= alpha * computeDerivativeOfCost(theta, j, X, y, m);
    end

    theta = temp_theta;

    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);
end

end

%Computes the partial derivative of the cost function with respect to the jth
%element of the theta vector.
function sum_of_errors = computePartialDerivativeOfCost(theta, j, X, y, m)
    sum_of_errors = 0;
    for i = 1:m
        %X(i,j) is the jth feature(jth col) of the ith training example(ith row)
        sum_of_errors += ( hypothesis( X(i, :), theta ) - y(i, :) ) * X(i, j);
    end
    sum_of_errors /= m;
end

%Computes the linear combination of the ith training example (ith row) of X, and
%the theta vector.
function product = hypothesis(x, theta)
    product = x * theta;
end
